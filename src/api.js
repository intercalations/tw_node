const _ = require('lodash');
const parseISO = require('date-fns/parseISO');

const Futures = require('./futures')
const Session = require('./session');
const Stream = require('./stream');

module.exports = class Api {
  constructor(username, password, apiUrl = null, streamUrl = null) {
    apiUrl = apiUrl || 'https://api.tastyworks.com'
    streamUrl = streamUrl || 'wss://tasty.dxfeed.com/live/cometd'

    this._session = new Session(username, password, apiUrl);
    this._stream = new Stream(this._session, streamUrl);

    this._urls = {
      futures: `${apiUrl}/instruments/futures`,
      futuresOptionChains: `${apiUrl}/futures-option-chains`,
      marketMetrics: `${apiUrl}/market-metrics`,
      optionChains: `${apiUrl}/option-chains`,
      search: `${apiUrl}/symbols/search`
    }
  }

  async futures(symbol) {
    symbol = symbol.toUpperCase().replace('/', '');

    if (symbol == 'VX') {
      return Futures.vxContracts();
    }

    let url = this._urls.futures;
    let response = await this._session.request(url);

    let contracts = response.body.data.items.filter(contract =>
      contract['product-code'] == symbol
    );

    let contractWithStreamerExchangeCode = _.find(contracts, contract =>
      !!contract['streamer-exchange-code']
    );

    contracts.forEach(contract => {
      let expiry = parseISO(contract['expiration-date']);
      let year = expiry.getFullYear().toString().slice(-2);

      contract['symbol'] = contract['symbol'].slice(0, -1) + year;
      contract['streamer-exchange-code'] =
        contractWithStreamerExchangeCode['streamer-exchange-code']
    });

    return contracts;
  }

  async futuresCurve(symbol) {
    let contracts = await this.futures(symbol);

    let streamerCodes = []
    let futures = []

    await contracts.forEach(async (contract) => {
      let code = `${contract.symbol}:${contract['streamer-exchange-code']}`;
      streamerCodes.push(code);

      futures.push({
        expiry: contract['expiration-date'],
        streamerCode: code,
        symbol: contract.symbol
      });
    });

    let futuresQuotes = await this.quotes(streamerCodes.sort());

    futuresQuotes.forEach(quote => {
      let contract = _.find(futures, contract =>
        contract.streamerCode == quote.eventSymbol
      )
      contract.midPrice = (quote.bidPrice + quote.askPrice) / 2

      delete contract.streamerCode;
    });

    return futures;
  }

  async futuresOptionChains(symbol) {
    symbol = encodeURIComponent(symbol.toUpperCase().replace('/', ''));
    let url = `${this._urls.futuresOptionChains}/${symbol}/nested`;
    let response = await this._session.request(url);

    return response.body.data;
  }

  async futuresOptionContracts(symbol, expiries) {
    symbol = symbol.toUpperCase().replace('/', '');

    let futures = await this.futures(symbol);
    if (futures.length == 0) {
      return {};
    }

    let exchangeCode = futures[0]['streamer-exchange-code'];

    let chains = await this.futuresOptionChains(symbol);
    let expirations = chains['option-chains'][0].expirations;

    let contracts = expirations.filter(contract =>
      _.includes(expiries, contract['expiration-date'])
    );

    let formattedContracts = contracts.flatMap(contract =>
      contract.strikes.flatMap(strike => {
        let base = {
          daysToExpiration: contract['days-to-expiration'],
          expiry: contract['expiration-date'],
          underlyingSymbol: symbol
        };

        // Example: ./NGH0 LNEH0 200225C11.75
        let regex = RegExp(
          '\\.' + // match "."
          '(\\/[A-Z]{2})[A-Z][0-9]\\s' + // match "/NGH0 ", capture "/NG"
          '([A-Z0-9]*)[0-9]\\s*' +       // match "LNEH0 ", capture "LNEH"
          '([0-9]{2})[0-9]*' +           // match "200225", capture "20"
          '([CP][0-9]*\\.?[0-9]*)'       // match "C11.75", capture "C11.75"
        );

        let strikePrice = parseFloat(strike['strike-price']);

        let callMatch = strike['call'].match(regex);
        let callSymbol =
          `./${callMatch[2]}${callMatch[3]}${callMatch[4]}:${exchangeCode}`;
        let call = { type: 'C', strike: strikePrice, symbol: callSymbol }

        let putMatch = strike['put'].match(regex);
        let putSymbol =
          `./${putMatch[2]}${putMatch[3]}${putMatch[4]}:${exchangeCode}`;
        let put = { type: 'P', strike: strikePrice, symbol: putSymbol }

        return [Object.assign(call, base), Object.assign(put, base)];
      })
    );

    let groups = _.groupBy(formattedContracts, 'symbol');
    let contractInfo = _.mapValues(groups, groupData =>
      ({ 'Info': groupData[0] })
    );

    let contractSymbols = _.map(formattedContracts, 'symbol');
    let response = await this._stream.optionsContracts(contractSymbols);

    return _.merge(response, contractInfo);
  }

  async marketMetrics(symbols) {
    symbols = symbols.map(symbol => symbol.toUpperCase());
    let url = `${this._urls.marketMetrics}?symbols=${symbols}`
    let response = await this._session.request(url);

    return response.body.data.items || [];
  }

  async optionChains(symbol) {
    symbol = encodeURIComponent(symbol.toUpperCase());
    let url = `${this._urls.optionChains}/${symbol}/nested`;
    let response = await this._session.request(url);

    return response.body.data.items[0];
  }

  async optionContracts(symbol, expiries) {
    symbol = symbol.toUpperCase();

    let chains = await this.optionChains(symbol);

    let contracts = chains.expirations.filter(contract =>
      _.includes(expiries, contract['expiration-date'])
    );

    let formattedContracts = contracts.flatMap(contract =>
      contract.strikes.flatMap(strike => {
        let base = {
          daysToExpiration: contract['days-to-expiration'],
          expiry: contract['expiration-date'],
          underlyingSymbol: symbol
        };

        let regex = RegExp(`${symbol}\\s+`);

        let strikePrice = parseFloat(strike['strike-price']);

        let [callExpiry, ] = strike['call'].replace(regex, '').split('C');
        let callSymbol = `.${symbol}${callExpiry}C${strikePrice}`;
        let call = { type: 'C', strike: strikePrice, symbol: callSymbol }

        let [putExpiry, ] = strike['put'].replace(regex, '').split('P');
        let putSymbol = `.${symbol}${putExpiry}P${strikePrice}`;
        let put = { type: 'P', strike: strikePrice, symbol: putSymbol }

        return [Object.assign(call, base), Object.assign(put, base)];
      })
    );

    let groups = _.groupBy(formattedContracts, 'symbol');
    let contractInfo = _.mapValues(groups, groupData =>
      ({ 'Info': groupData[0] })
    );

    let contractSymbols = _.map(formattedContracts, 'symbol');
    let response = await this._stream.optionsContracts(contractSymbols);

    return _.merge(response, contractInfo);
  }

  async optionExpiries(symbol) {
    let response = await this.marketMetrics([symbol]);

    if (response.length == 0) {
      return []
    }

    let expiries = _.map(
      response[0]['option-expiration-implied-volatilities'], 'expiration-date'
    ).sort();

    return expiries;
  }

  async quotes(symbols) {
    symbols = symbols.map(symbol => symbol.toUpperCase());
    let response = await this._stream.quotes(symbols);

    return response;
  }

  async search(symbol) {
    symbol = encodeURIComponent(symbol.toUpperCase());
    let url = `${this._urls.search}/${symbol}`;
    let response = await this._session.request(url);

    return response.body.data.items;
  }
}
