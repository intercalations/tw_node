const got = require('got');

module.exports = class Session {
  constructor(username, password, url = 'https://api.tastyworks.com') {
    this._username = username;
    this._password = password;

    this._loggedIn = false;
    this._loggedInAt = null;
    this._sessionToken = null;

    this._urls = {
      quoteStreamerTokens: `${url}/quote-streamer-tokens`,
      sessions: `${url}/sessions`,
      sessionsValidate: `${url}/sessions/validate`
    };
  }

  isLoginStale() {
    if (!this._loggedIn || !this._loggedInAt) {
      return true;
    }

    let differenceInSeconds = (Date.now() - this._loggedInAt) / 1000;
    return differenceInSeconds > 60;
  }

  async quoteStreamerTokens() {
    let url = this._urls.quoteStreamerTokens;
    let response = await this.request(url);

    return response.body.data;
  }

  async request(url, options = {}) {
    let defaultOptions = { responseType: 'json' };
    let headers = await this.requestHeaders();
    let requestOptions = Object.assign({}, defaultOptions, headers, options);
    let response = await got(url, requestOptions);
    return response;
  }

  async requestHeaders() {
    let authorizationToken = await this.sessionToken;
    return { headers: { Authorization: authorizationToken  } };
  }

  get sessionToken() {
    return (async () => {
      if (this._loggedIn && this._sessionToken && !this.isLoginStale()) {
        return this._sessionToken;
      }

      await this.updateSessionToken();

      return this._sessionToken;
    })();
  }

  async updateSessionToken() {
    let credentials = {
      login: this._username,
      password: this._password
    };

    let response = await got.post(
      this._urls.sessions,
      { json: credentials, responseType: 'json' }
    );

    this._loggedIn = true;
    this._loggedInAt = Date.now();
    this._sessionToken = response.body.data['session-token'];

    await this.validateSession();
  }

  async validateSession() {
    await this.request(this._urls.sessionsValidate, { method: 'post' } );

    return true;
  }
}
