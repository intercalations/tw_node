const WebSocketAsPromised = require('websocket-as-promised');
const W3CWebSocket = require('websocket').w3cwebsocket;
const _ = require('lodash');

// TODO: Reference: https://tools.dxfeed.com/webservice/qtable-demo.jsp
// TODO: Reference: https://www.dxfeed.com/changes-in-cme-and-cfe-futures-symbology/

module.exports = class Stream {
  constructor(session, streamUrl) {
    this._session = session;

    this._streamToken = null;
    this._streamTokenCreatedAt = null;

    this._clientId = null;

    this._socket = new WebSocketAsPromised(streamUrl, {
      createWebSocket: url => new W3CWebSocket(url),
      packMessage: data => JSON.stringify(data),
      unpackMessage: data => JSON.parse(data),
      attachRequestId: (data, requestId) =>
        [Object.assign({ id: (requestId).toString() }, data)],
      extractRequestId: data => data && data.length > 0 && data[0].id
    });

    this._channels = {
      connect: '/meta/connect',
      handshake: '/meta/handshake',
      data: '/service/data',
      subscription: '/service/sub'
    };
  }

  addDataSubscriptions(subscriptions) {
    let channel = this._channels.subscription;
    let message = {
      data: {
        'add': subscriptions
      }
    };

    this.sendMessage(channel, message);
  }

  addOptionsSubscription(symbols) {
    this.addDataSubscriptions({
      'Greeks': symbols,
      'Quote': symbols,
      'TheoPrice': symbols
    });
  }

  addQuotesSubscription(symbols) {
    this.addDataSubscriptions({ 'Quote': symbols });
  }

  async handshake() {
    let authorizationToken = await this.streamToken;
    let message = {
      channel: this._channels.handshake,
      ext: {
        'com.devexperts.auth.AuthToken': authorizationToken
      },
      version: '1.0',
      minimumVersion: '1.0',
      supportedConnectionTypes: ['websocket'],
      advice: {
        'timeout': 60000,
        'interval': 0
      }
    };
    return await this._socket.sendRequest(message);
  }

  isStreamTokenStale() {
    if (!this._streamTokenCreatedAt) {
      return true;
    }

    let differenceInSeconds = (Date.now() - this._streamTokenCreatedAt) / 1000;
    return differenceInSeconds > 60;
  }

  optionsContracts(symbols) {
    return new Promise((resolve) => {
      let keys = ['Greeks', 'Quote', 'TheoPrice'];
      let parsedContracts = _.fromPairs(_.zipWith(keys, key => [key, []]));
      let columns = _.fromPairs(_.zipWith(keys, key => [key, []]));

      let allContracts = {};

      this._socket.onOpen.addListener(async () => {
        let response = await this.handshake();
        this._clientId = response[0].clientId;

        await this.resetSubscriptions();
        await this.pollServer();
        await this.addOptionsSubscription(symbols);
        await this.pollServer();
      });

      this._socket.onUnpackedMessage.addListener(async (data) => {
        var parsedMessage, messageType, allDataReceived;

        let message = data[0];

        switch (message.channel) {
          case this._channels.data:

            if (Array.isArray(message.data[0])) {
              messageType = message.data[0][0];
            } else {
              messageType = message.data[0];
            }

            [parsedMessage, columns[messageType]] =
              this.parseDataMessage(message, columns[messageType]);
            parsedContracts[messageType] =
              parsedContracts[messageType].concat(parsedMessage);

            allDataReceived = _.every(keys, key => {
              let contractSymbols =
                _.uniq(_.map(parsedContracts[key], 'eventSymbol'));

              return _.difference(symbols, contractSymbols).length == 0;
            })

            if (allDataReceived) {
              await this._socket.close();

              keys.forEach(key => {
                parsedContracts[key] =
                  _.uniqBy(parsedContracts[key].reverse(), 'eventSymbol');

                let groups = _.groupBy(parsedContracts[key], 'eventSymbol');
                let flatContracts = _.mapValues(groups, groupData =>
                  ({ [key]: groupData[0] })
                );

                allContracts = _.merge(allContracts, flatContracts);
              });

              resolve(allContracts);
            }

            break;
          default:
            break;
        }
      });

      this._socket.open();
    });
  }

  parseDataMessage(message, columns) {
    if (columns.length == 0) {
      columns = message.data[0][1];
    }

    let values = message.data[1];
    let rows = _.chunk(values, columns.length);

    return [rows.map(row => _.fromPairs(_.zip(columns, row))), columns];
  }

  pollServer() {
    this.sendMessage(this._channels.connect, {});
  }

  quotes(symbols) {
    return new Promise((resolve) => {
      let allQuotes = [];
      let columns = [];

      this._socket.onOpen.addListener(async () => {
        let response = await this.handshake();
        this._clientId = response[0].clientId;

        await this.resetSubscriptions();
        await this.pollServer();
        await this.addQuotesSubscription(symbols);
        await this.pollServer();
      });

      this._socket.onUnpackedMessage.addListener(async (data) => {
        var messageQuotes, quoteSymbols;

        let message = data[0];

        switch (message.channel) {
          case this._channels.data:
            [messageQuotes, columns] = this.parseDataMessage(message, columns);
            allQuotes = allQuotes.concat(messageQuotes);
            quoteSymbols = _.uniq(_.map(allQuotes, 'eventSymbol'));

            if (_.difference(symbols, quoteSymbols).length == 0) {
              await this._socket.close();
              resolve(_.uniqBy(allQuotes.reverse(), 'eventSymbol'));
            }

            break;
          default:
            break;
        }
      });

      this._socket.open();
    });
  }

  resetSubscriptions() {
    let channel = this._channels.subscription;
    let message = {
      data: {
        'reset': true
      }
    };

    this.sendMessage(channel, message);
  }

  sendMessage(channel, message) {
    let channelMessage = Object.assign(
      {}, message, { channel: channel, clientId: this._clientId }
    );

    this._socket.sendPacked([channelMessage]);
  }

  get streamToken() {
    return (async () => {
      if (this._streamToken && !this.isStreamTokenStale()) {
        return this._streamToken;
      }

      await this.updateStreamToken();

      return this._streamToken;
    })();
  }

  async updateStreamToken() {
    let response = await this._session.quoteStreamerTokens();

    this._streamToken = response.token;
    this._streamTokenCreatedAt = Date.now();
  }
}
