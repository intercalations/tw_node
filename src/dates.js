const addWeeks = require('date-fns/addWeeks');
const getDay = require('date-fns/getDay');
const setDay = require('date-fns/setDay');
const startOfMonth = require('date-fns/startOfMonth')

module.exports.nthDayOfMonth = function(date, dayOfWeek, n) {
  let startDate = startOfMonth(date);
  let firstDay = setDay(startDate, dayOfWeek, { weekStartsOn: getDay(startDate) });
  return addWeeks(firstDay, n - 1);
}
