const addMonths = require('date-fns/addMonths');
const eachMonthOfInterval = require('date-fns/eachMonthOfInterval');
const formatISO = require('date-fns/formatISO');
const isPast = require('date-fns/isPast');
const subDays = require('date-fns/subDays');

const Dates = require('./dates');

module.exports.monthCode = function(date) {
  switch (date.getMonth() + 1) {
    case 1:
      return 'F';
    case 2:
      return 'G';
    case 3:
      return 'H';
    case 4:
      return 'J';
    case 5:
      return 'K';
    case 6:
      return 'M';
    case 7:
      return 'N';
    case 8:
      return 'Q';
    case 9:
      return 'U';
    case 10:
      return 'V';
    case 11:
      return 'X';
    case 12:
      return 'Z';
  }
}

module.exports.vxContracts = function() {
  return module.exports.vxExpiries().map((expiry, index) => {
    let code = module.exports.monthCode(expiry);
    let year = expiry.getFullYear().toString().slice(-2);

    let expirationDate = formatISO(expiry, { representation: 'date' });
    let activeMonth = index == 0;

    return {
      'symbol': `/VX${code}${year}`,
      'product-code': 'VX',
      'contract-size': '1000.0',
      'tick-size': '0.05',
      'notional-multiplier': '1000.0',
      'main-fraction': '0.0',
      'sub-fraction': '0.0',
      'display-factor': '0.01',
      'last-trade-date': expirationDate,
      'expiration-date': expirationDate,
      'closing-only-date': expirationDate,
      'product-group': 'CFE_VX',
      'active': true,
      'active-month': activeMonth,
      'is-closing-only': false,
      'future-etf-equivalent': null,
      'exchange': 'CFE',
      'tick-sizes': [
        {
          'value': '0.05'
        }
      ],
      'option-tick-sizes': [
        {
          'value': '0.05'
        }
      ],
      'streamer-exchange-code': 'XCBF'
    }
  });
}

module.exports.vxExpiries = function() {
  let today = new Date();

  let months = eachMonthOfInterval({
    start: today,
    end: addMonths(today, 10)
  });

  let expiries = months.map(month => {
    let nextMonth = addMonths(month, 1);
    let friday = Dates.nthDayOfMonth(nextMonth, 5, 3);
    return subDays(friday, 30);
  });

  if (isPast(expiries[0])) {
    return expiries.slice(1);
  } else {
    return expiries.slice(0, -1);
  }
}
