# tw_node

[tastyworks](https://tastyworks.com/) is a brokerage whose competitive pricing
makes their platform attractive to algorithmic traders. Unfortunately,
tastyworks has no official API. This project offers a tool for obtaining data
from their brokerage platform.

# Getting Started

```sh
npm install
cp .env.example .env
```

You'll then need to supply your account credentials in `.env`.

# Usage

## Futures

### Contracts

To obtain general information about futures contracts, use the following
command:

```sh
futures <symbol>
```

For example, running `bin/tw futures /ES` will return something like this:

```json
[
  {
    "symbol":"/ESM21",
    "product-code":"ES",
    "contract-size":"50.0",
    "tick-size":"0.25",
    "notional-multiplier":"50.0",
    "main-fraction":"0.0",
    "sub-fraction":"0.0",
    "display-factor":"0.01",
    "last-trade-date":"2021-06-18",
    "expiration-date":"2021-06-18",
    "closing-only-date":"2021-06-18",
    "active":true,
    "active-month":true,
    "next-active-month":false,
    "is-closing-only":false,
    "stops-trading-at":"2021-06-18T13:30:00.000+00:00",
    "expires-at":"2021-06-18T13:30:00.000+00:00",
    "product-group":"CME_ES",
    "exchange":"CME",
    "roll-target-symbol":"/ESU1",
    "streamer-exchange-code":"XCME",
    "back-month-first-calendar-symbol":true,
    "future-etf-equivalent":{
      "symbol":"SPY",
      "share-quantity":500
    },
    "tick-sizes":[
      {
        "value":"0.25"
      }
    ],
    "option-tick-sizes":[
      {
        "value":"0.05",
        "threshold":"5.0"
      },
      {
        "value":"0.25"
      }
    ],
    "spread-tick-sizes":[
      {
        "value":"0.05",
        "symbol":"/ESU1"
      },
      {
        "value":"0.05",
        "symbol":"/ESZ1"
      }
    ]
  },
  ...
]
```

### Curve

To obtain the prices and expiries of all contracts under a given symbol, use
the following command:

```sh
futures-curve <symbol>
```

For example, running `bin/tw futures-curve /ES` will return something like
this:

```json
[
  {
    "expiry":"2021-06-18",
    "symbol":"/ESM21",
    "midPrice":4165.875
  },
  {
    "expiry":"2021-09-17",
    "symbol":"/ESU21",
    "midPrice":4160
  },
  {
    "expiry":"2021-12-17",
    "symbol":"/ESZ21",
    "midPrice":4137.5
  }
]
```

## Futures Options

### Chains

To obtain the futures options chains for a given symbol, use the following
command:

```sh
futures-option-chains <symbol>
```

For example, running `bin/tw futures-option-chains /NG` will return something
like this:

```json
{
  "futures":[
    {
      "symbol":"/NGV1",
      "root-symbol":"/NG",
      "expiration-date":"2021-09-28",
      "days-to-expiration":157,
      "active-month":false,
      "next-active-month":false,
      "stops-trading-at":"2021-09-28T18:30:00.000+00:00",
      "expires-at":"2021-09-28T18:30:00.000+00:00"
    },
    ...
  ],
  "option-chains":[
    {
      "underlying-symbol":"/NG",
      "root-symbol":"/NG",
      "exercise-style":"American",
      "expirations":[
        {
          "underlying-symbol":"/NGV1",
          "root-symbol":"/NG",
          "option-root-symbol":"LNE",
          "option-contract-symbol":"LNEV1",
          "asset":"LNE",
          "expiration-date":"2021-09-27",
          "days-to-expiration":156,
          "notional-value":"1.0",
          "display-factor":"0.0001",
          "strike-factor":"10.0",
          "stops-trading-at":"2021-09-27T18:30:00.000+00:00",
          "expires-at":"2021-09-27T18:30:00.000+00:00",
          "tick-sizes":[
            {
              "value":"0.001"
            }
          ],
          "strikes":[
            {
              "strike-price":"5.0",
              "call":"./NGV1 LNEV1 210927C5",
              "put":"./NGV1 LNEV1 210927P5"
            },
            ...
          ]
        },
        ...
      ]
    }
  ]
}
```

### Contracts

To obtain detailed information about the futures options contracts for a given
symbol and a list of expiries, use the following command:

```sh
futures-option-contracts <symbol> <expiries>
```

Note that `<expiries>` can contain one or more expiries
(in `YYYY-MM-DD` format) separated by commas.

For example, running `bin/tw futures-option-contracts /NG "2021-09-27"` will
return something like this:


```json
{
  "./LNEV21C4.5:XNYM":{
    "Greeks":{
      "eventSymbol":"./LNEV21C4.5:XNYM",
      "eventTime":0,
      "eventFlags":0,
      "index":6954460759929651000,
      "time":1619211575000,
      "sequence":0,
      "price":0.01178903,
      "volatility":0.3656343,
      "delta":0.04470488,
      "gamma":0.13472918,
      "theta":-0.00020787,
      "rho":0.00051194,
      "vega":0.00180293
    },
    "Quote":{
      "eventSymbol":"./LNEV21C4.5:XNYM",
      "eventTime":0,
      "sequence":0,
      "timeNanoPart":0,
      "bidTime":0,
      "bidExchangeCode":"G",
      "bidPrice":0.008,
      "bidSize":100,
      "askTime":0,
      "askExchangeCode":"G",
      "askPrice":0.013,
      "askSize":100
    },
    "TheoPrice":{
      "eventSymbol":"./LNEV21C4.5:XNYM",
      "eventTime":0,
      "eventFlags":0,
      "index":6954460759929651000,
      "time":1619211575000,
      "sequence":0,
      "price":0.011789032606073,
      "underlyingPrice":2.906,
      "delta":0.0447032553699538,
      "gamma":0.1347264605440896,
      "dividend":0.00239968288767378,
      "interest":0.002158894346192764
    },
    "Info":{
      "type":"C",
      "strike":4.5,
      "symbol":"./LNEV21C4.5:XNYM",
      "daysToExpiration":156,
      "expiry":"2021-09-27",
      "underlyingSymbol":"NG"
    }
  },
  ...
}
```

## Options

### Chains

To obtain the options chains for a given symbol, use the following command:

```sh
option-chains <symbol>
```

For example, running `bin/tw option-chains V` will return something
like this:

```json
{
  "underlying-symbol":"V",
  "root-symbol":"V",
  "option-chain-type":"Standard",
  "shares-per-contract":100,
  "tick-sizes":[
    {
      "value":"0.01",
      "threshold":"3.0"
    },
    {
      "value":"0.05"
    }
  ],
  "deliverables":[
    {
      "id":84252,
      "root-symbol":"V",
      "deliverable-type":"Shares",
      "description":"100 shares of V",
      "amount":"100.0",
      "symbol":"V",
      "instrument-type":"Equity",
      "percent":"100"
    }
  ],
  "expirations":[
    {
      "expiration-type":"Weekly",
      "expiration-date":"2021-04-30",
      "days-to-expiration":5,
      "settlement-type":"PM",
      "strikes":[
        {
          "strike-price":"110.0",
          "call":"V     210430C00110000",
          "put":"V     210430P00110000"
        },
        ...
      ]
    },
    ...
  ]
}
```

### Expiries

To obtain a list of expiries for the available options contracts for a given
symbol, use the following command:

```sh
option-expiries <symbol>
```

For example, running `bin/tw option-expiries FISV` will return something like
this:

```json
[
  "2021-04-23",
  "2021-04-30",
  "2021-05-07",
  "2021-05-14",
  "2021-05-21",
  "2021-05-28",
  "2021-06-04",
  "2021-06-18",
  "2021-09-17",
  "2021-12-17",
  "2022-01-21",
  "2023-01-20"
]
```

### Contracts

To obtain detailed information about the options contracts for a given symbol
and a list of expiries, use the following command:

```sh
option-contracts <symbol> <expiries>
```

Note that `<expiries>` can contain one or more expiries
(in `YYYY-MM-DD` format) separated by commas.

For example, running `bin/tw option-contracts FISV "2021-04-30"` will
return something like this:

```json
{
  ".FISV210430P111":{
    "Greeks":{
      "eventSymbol":".FISV210430P111",
      "eventTime":0,
      "eventFlags":0,
      "index":6954596944752673000,
      "time":1619243283000,
      "sequence":0,
      "price":0.14642904,
      "volatility":0.4953022,
      "delta":-0.03889211,
      "gamma":0.00923393,
      "theta":-0.04908472,
      "rho":-0.00107979,
      "vega":0.0155112
    },
    "Quote":{
      "eventSymbol":".FISV210430P111",
      "eventTime":0,
      "sequence":0,
      "timeNanoPart":0,
      "bidTime":0,
      "bidExchangeCode":"X",
      "bidPrice":0.05,
      "bidSize":20,
      "askTime":0,
      "askExchangeCode":"X",
      "askPrice":0.18,
      "askSize":30
    },
    "TheoPrice":{
      "eventSymbol":".FISV210430P111",
      "eventTime":0,
      "eventFlags":0,
      "index":6954596944752673000,
      "time":1619243283000,
      "sequence":0,
      "price":0.1464290418412541,
      "underlyingPrice":125.81,
      "delta":-0.0388909967718157,
      "gamma":0.00923379214481662,
      "dividend":0,
      "interest":0
    },
    "Info":{
      "type":"P",
      "strike":111,
      "symbol":".FISV210430P111",
      "daysToExpiration":5,
      "expiry":"2021-04-30",
      "underlyingSymbol":"FISV"
    }
  },
  ...
}
```

## Metrics

To obtain market metrics for a given symbol, use the following command:

```sh
metrics <symbols>
```

Note that `<symbols>` can contain one or more symbols separated by commas.

For example, running `bin/tw metrics AMD` will return something like this:

```json
[
  {
    "symbol":"AMD",
    "implied-volatility-index":"0.479288465",
    "implied-volatility-index-5-day-change":"0.012213855",
    "implied-volatility-index-rank":"0.18754946",
    "tos-implied-volatility-index-rank":"0.18754946",
    "tw-implied-volatility-index-rank":"0.257794172",
    "tos-implied-volatility-index-rank-updated-at":"2021-04-23T19:47:11.640Z",
    "implied-volatility-index-rank-source":"tos",
    "implied-volatility-percentile":"0.134681894",
    "implied-volatility-updated-at":"2021-04-23T19:47:11.639Z",
    "liquidity-value":"140285.383420245",
    "liquidity-rank":"0.350251579",
    "liquidity-rating":4,
    "created-at":"2019-01-20T18:02:42.011-06:00",
    "updated-at":"2021-04-23T19:47:12.451Z",
    "option-expiration-implied-volatilities":[
      {
        "expiration-date":"2021-04-23",
        "option-chain-type":"Standard",
        "settlement-type":"PM",
        "implied-volatility":"1.1597609"
      },
      ...
    ],
    "liquidity-running-state":{
      "sum":"0.0",
      "count":0,
      "started-at":"2021-04-24T10:00:15.549Z"
    },
    "beta":"1.782935",
    "beta-updated-at":"2021-04-18T17:00:28.362Z",
    "corr-spy-3month":"0.63",
    "dividend-rate-per-share":"0.0",
    "dividend-yield":"0.0",
    "earnings":{
      "visible":true,
      "expected-report-date":"2021-04-27",
      "estimated":false,
      "time-of-day":"AMC",
      "late-flag":0,
      "quarter-end-date":"2021-03-01",
      "actual-eps":"0.14",
      "consensus-estimate":"0.38",
      "updated-at":"2021-04-24T03:00:03.025Z"
    },
    "listed-market":"XNAS",
    "lendability":"Easy To Borrow",
    "borrow-rate":"1.25"
  }
]
```

## Quotes

To obtain quotes for a given list of symbols, use the following command:

```sh
quote <symbols>
```

Note that `<symbols>` can contain one or more symbols separated by commas.

For example, running `bin/tw quote MSFT,GOOG` will return something like this:

```json
[
  {
    "eventSymbol":"GOOG",
    "eventTime":0,
    "sequence":0,
    "timeNanoPart":0,
    "bidTime":0,
    "bidExchangeCode":"Q",
    "bidPrice":2319,
    "bidSize":4,
    "askTime":0,
    "askExchangeCode":"Q",
    "askPrice":2326,
    "askSize":1
  },
  {
    "eventSymbol":"MSFT",
    "eventTime":0,
    "sequence":0,
    "timeNanoPart":0,
    "bidTime":0,
    "bidExchangeCode":"P",
    "bidPrice":261.6,
    "bidSize":4,
    "askTime":0,
    "askExchangeCode":"Q",
    "askPrice":261.96,
    "askSize":1
  }
]
```

## Searching

To search for a symbol, use the following command:

```sh
search <symbol>
```

For example, running `bin/tw search AAP` will return something like this:

```json
[
  {
    "symbol":"AAP",
    "description":"Advance Auto Parts Inc Advance Auto Parts Inc W/I"
  },
  {
    "symbol":"AAPL",
    "description":"Apple Inc. - Common Stock"
  }
]
```
