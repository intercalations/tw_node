#!/usr/bin/env node

const path = require('path');

const dotenv = require('dotenv').config({
  path: path.resolve(__dirname,  '..', '.env')
});
const program = require('commander');

const packageJson = require('../package.json');

const Api = require('../src/api');


if (dotenv.error) {
  throw dotenv.error;
}

let username = dotenv.parsed['USERNAME'];
let password = dotenv.parsed['PASSWORD'];
let twApi = new Api(username, password);

async function futures(symbol) {
  try {
    let result = await twApi.futures(symbol);
    console.log(JSON.stringify(result));
  } catch (error) {
    console.log(error);
    process.exit(1)
  }
}

async function futuresCurve(symbol) {
  try {
    let result = await twApi.futuresCurve(symbol);
    console.log(JSON.stringify(result));
  } catch (error) {
    console.log(error);
    process.exit(1)
  }
}

async function futuresOptionChains(symbol) {
  try {
    let result = await twApi.futuresOptionChains(symbol);
    console.log(JSON.stringify(result));
  } catch (error) {
    console.log(error);
    process.exit(1)
  }
}

async function futuresOptionContracts(symbol, expiries) {
  expiries = expiries.split(',');

  try {
    let result = await twApi.futuresOptionContracts(symbol, expiries);
    console.log(JSON.stringify(result));
  } catch (error) {
    console.log(error);
    process.exit(1)
  }
}

async function marketMetrics(symbols) {
  symbols = symbols.split(',');

  try {
    let result = await twApi.marketMetrics(symbols);
    console.log(JSON.stringify(result));
  } catch (error) {
    console.log(error);
    process.exit(1)
  }
}

async function optionChains(symbol) {
  try {
    let result = await twApi.optionChains(symbol);
    console.log(JSON.stringify(result));
  } catch (error) {
    console.log(error);
    process.exit(1)
  }
}

async function optionContracts(symbol, expiries) {
  expiries = expiries.split(',');

  try {
    let result = await twApi.optionContracts(symbol, expiries);
    console.log(JSON.stringify(result));
  } catch (error) {
    console.log(error);
    process.exit(1)
  }
}

async function optionExpiries(symbol) {
  try {
    let result = await twApi.optionExpiries(symbol);
    console.log(JSON.stringify(result));
  } catch (error) {
    console.log(error);
    process.exit(1)
  }
}

async function quote(symbols) {
  symbols = symbols.split(',');

  try {
    let result = await twApi.quotes(symbols);
    console.log(JSON.stringify(result));
  } catch (error) {
    console.log(error);
    process.exit(1)
  }
}

async function search(symbol) {
  try {
    let result = await twApi.search(symbol);
    console.log(JSON.stringify(result));
  } catch (error) {
    console.log(error);
    process.exit(1)
  }
}

(async() => {
  program
    .version(packageJson.version);

  program
    .command('futures <symbol>')
    .description('retrieves futures contract information for the supplied symbol')
    .action(futures);

  program
    .command('futures-curve <symbol>')
    .description('retrieves futures contract prices and expiries for the supplied symbol')
    .action(futuresCurve);

  program
    .command('futures-option-chains <symbol>')
    .description('retrieves the futures option chains for the supplied symbol')
    .action(futuresOptionChains);

  program
    .command('futures-option-contracts <symbol> <expiries>')
    .description(
      'retrieves the futures option contracts for the supplied symbol and expiries'
    )
    .action(futuresOptionContracts);

  program
    .command('metrics <symbols>')
    .description('retrieves market metrics for the supplied symbols')
    .action(marketMetrics);

  program
    .command('option-chains <symbol>')
    .description('retrieves the option chains for the supplied symbol')
    .action(optionChains);

  program
    .command('option-contracts <symbol> <expiries>')
    .description(
      'retrieves the option contracts for the supplied symbol and expiries'
    )
    .action(optionContracts);

  program
    .command('option-expiries <symbol>')
    .description('retrieves the option expiries for the supplied symbol')
    .action(optionExpiries);

  program
    .command('quote <symbols>')
    .description('retrieves quotes for the supplied symbols')
    .action(quote);

  program
    .command('search <symbol>')
    .description('searches for a symbol and retrieves information')
    .action(search);

  await program.parseAsync(process.argv);

  if (!program.args.length) {
    program.help();
  }
})();
